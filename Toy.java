public class Toy{
	
	private String name;
	private String type;
	private double cost;

	public void showType(){
		System.out.println("This toy is a " + this.type + ".");
	}
	
	public void setType(String newType){
		this.type = newType;
	}
	
	public void setCost(double newCost){
		this.cost = newCost;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getType(){
		return this.type;
	}
	
	public double getCost(){
		return this.cost;
	}
	
	public Toy(String name, String type, double cost){
		this.name = name;
		this.type = type;
		this.cost = 0;
	}
}