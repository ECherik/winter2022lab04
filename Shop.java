import java.util.Scanner;
public class Shop{
	public static void main(String[]args){
		Scanner reader = new Scanner(System.in);
		Toy[] newToy = new Toy[4];
		
		for(int i = 0; i < newToy.length; i++){
			String name;
			String type;
			double cost;
			
			System.out.println("What is the name of the new toy?");
			name = reader.nextLine();
			
			System.out.println("What is the type of the new toy?");
			type = reader.nextLine();
			
			System.out.println("What is the cost of the new toy?");
			cost = Double.parseDouble(reader.nextLine());
			
			newToy[i] = new Toy(name, type, cost);
		}
		
		System.out.println("Update the type and cost of the 4th toy.");
		System.out.println("What is the type of the 4th toy?");
		newToy[3].setType(reader.nextLine());
		System.out.println("What is the cost of the 4th toy?");
		newToy[3].setCost(Double.parseDouble(reader.nextLine()));
		
		System.out.println("Information about the 4th toy");
		System.out.println(newToy[3].getName());
		System.out.println(newToy[3].getType());
		System.out.println(newToy[3].getCost());
		
		newToy[3].showType();
	}
}